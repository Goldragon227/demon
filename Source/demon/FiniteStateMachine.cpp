// Fill out your copyright notice in the Description page of Project Settings.

#include "FiniteStateMachine.h"
#include"Engine/GameEngine.h"

// Sets default values for this component's properties
UFiniteStateMachine::UFiniteStateMachine()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	isRunning = false;

	// ...
	States = new TMap<uint8, FSMState>();
}


bool UFiniteStateMachine::Add(uint8 Key, FSMState State)
{
	if (States->Contains(Key)) {
		return false;
	}
	else {
		States->Add(Key, State);
		return true;
	}
}

FSMState * UFiniteStateMachine::GetState(uint8 Key)
{
	if (States->Contains(Key)) {
		return States->Find(Key);
	}
	return NULL;
}

bool UFiniteStateMachine::ChangeState(uint8 Key)
{
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, FString::SanitizeFloat(Key));
	}
	if (States->Contains(Key)) {
		//OnExit
		if (CurrentState != nullptr) {
			CurrentState->OnExit.ExecuteIfBound();
		}
		
		CurrentState = States->Find(Key);
		CurrentStateName = Key;
		//OnEnter
		if (isRunning) {
			CurrentState->OnEnter.ExecuteIfBound();
		}
		//OnUpdate
		CurrentAction = CurrentState->OnUpdate;
		return true;
	}
	else {
		return false;
	}
}

bool UFiniteStateMachine::Start(uint8 Key)
{
	if (ChangeState(Key) && !isRunning) {
		isRunning = true;
		CurrentState->OnEnter.ExecuteIfBound();
		return true;
	}
	return false;
	
}

bool UFiniteStateMachine::Resume()
{
	if (CurrentState != NULL) {
		isRunning = true;
		return true;
	}
	return false;
}

void UFiniteStateMachine::Stop()
{
	isRunning = false;
}

uint8 UFiniteStateMachine::GetCurrentState() {
	return CurrentStateName;
}

// Called when the game starts
void UFiniteStateMachine::BeginPlay()
{
	Super::BeginPlay();
	// ...
	
}


// Called every frame
void UFiniteStateMachine::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (isRunning) {
		CurrentAction.ExecuteIfBound();
	}
	// ...
}

