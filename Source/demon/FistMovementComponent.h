// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "FistMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class DEMON_API UFistMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()


	void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;
	
};
