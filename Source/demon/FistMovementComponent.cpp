// Fill out your copyright notice in the Description page of Project Settings.

#include "FistMovementComponent.h"
#include "Engine/GameEngine.h"

void UFistMovementComponent::OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode)
{
	Super::OnMovementModeChanged(PreviousMovementMode, PreviousCustomMode);

	//if (GEngine) {
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Movement Mode Changed"));
	//}
}
