// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FistCharacter.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EFistCharacterEnum : uint8
{
	FC_Ground				UMETA(DisplayName = "Ground"),
	FC_Dash 				UMETA(DisplayName = "Dash"),
	FC_BoostCharge			UMETA(DisplayName = "BoostCharge"),
	FC_BoostPunch			UMETA(DisplayName = "BoostPunch"),
	FC_BoostPunchReverse 	UMETA(DisplayName = "BoostPunchReverse"),
	FC_BoostPunchDown 		UMETA(DisplayName = "BoostPunchDown"),
	FC_BoostPunchUp			UMETA(DisplayName = "BoostPunchUp"),
	FC_Wallrun				UMETA(DisplayName = "Wallrun"),
	FC_Wallstick			UMETA(DisplayName = "Wallstick")
};

USTRUCT(BlueprintType)
struct FCharacterIKHeadBounds {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	float YawMin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	float YawMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	float PitchMin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	float PitchMax;
};

UCLASS(config=Game)
class AFistCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = FistCharacter, meta = (AllowPrivateAccess = "true"))
	class UFiniteStateMachine* stateMachine;

public:
	//AFistCharacter();
	AFistCharacter(const FObjectInitializer& ObjectInitializer);

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool isDashing;

	

	

	UFUNCTION(BlueprintCallable)
	float IKFootTrace(float TraceDistance, FName SocketName);

	UFUNCTION(BlueprintCallable)
	FRotator IKGetHeadRotation(FName HeadSocketName, FVector LookAtPoint);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = IK)
	float IKTraceDistance;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IK)
	FName IKLeftFootName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IK)
	FName IKRightFootName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IK)
	float IKLeftFootLength;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IK)
	float IKRightFootLength;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = IK)
	float IKInterpSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IK)
	float CapsuleHalfHeightScaled;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = IKHead)
	FName IKHeadName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = IKHead)
	FRotator IKHeadLookRotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	FRotator IKHeadAdjustment;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	float IKHeadInterpSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	FVector IKHeadLookLocation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = IKHead)
	FCharacterIKHeadBounds HeadBounds;

	void Tick(float DeltaTime) override;

protected:

	UCapsuleComponent * CapsuleComponent;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void Move(float Value, EAxis::Type Type);

	UFUNCTION(BlueprintCallable)
	void Dodge();

	UFUNCTION(BlueprintCallable)
	void StopDodging();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ChargePunch)
	float ChargePunchSpeed;

	UFUNCTION(BlueprintCallable)
	void ChargePunch();

	UFUNCTION(BlueprintCallable)
	void StopChargePunch();

	UPROPERTY(BlueprintReadOnly)
	bool isPunchCharging;

	UPROPERTY(BlueprintReadWrite)
	float MaxPunchDistance;

	UPROPERTY(BlueprintReadWrite)
	float MaxPunchChargeTime;

	UFUNCTION(BlueprintCallable)
	void BoostPunch();

	UFUNCTION(BlueprintCallable)
	void BoostPunchReverse();

	UFUNCTION(BlueprintCallable)
		void PunchUp();

	UFUNCTION(BlueprintCallable)
		void PunchDown();


#pragma region Statemachine
	void GroundUpdate();

	FVector LastNonZeroMovementInput;

	//Charging Punch to boost
	void BoostChargeUpdate();
	void BoostChargeEnter();
	void BoostChargeExit();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = FistCharacter)
	bool isBoostCharging;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ChargePunch)
	float ChargeTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ChargePunch)
	float PunchVerticalTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ChargePunch)
	float PunchVerticalSpeed;

	//While Boosting
	void BoostPunchEnter();
	void BoostPunchUpdate();
	void BoostPunchExit();

	void BoostPunchEnter(int DirModifier);
	void BoostPunchUpdate(int DirModifier);
	void BoostPunchExit(int DirModifier);

	void BoostPunchReverseEnter();
	void BoostPunchReverseUpdate();
	void BoostPunchReverseExit();

	void BoostVerticalEnter(int DirModifier);
	void BoostVerticalExit(int DirModifier);
	void BoostVerticalUpdate(int DirModifier);

	void BoostVerticalUpEnter();
	void BoostVerticalUpExit();
	void BoostVerticalUpUpdate();

	void BoostVerticalDownEnter();
	void BoostVerticalDownExit();
	void BoostVerticalDownUpdate();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = FistCharacter)
	bool isBoostPunching;

	float OldGroundFriction;
	float OldBrakingFriction;
	FTimerHandle BoostPunchTimerHandle;
	void BoostPunchCancel();

	UPROPERTY(BlueprintReadWrite)
		float DashTime;
	UPROPERTY(BlueprintReadWrite)
		int DashSpeed;
	UPROPERTY(BlueprintReadWrite)
		float DashAcceleration;

	int OldWalkSpeed;
	float OldWalkAcceleration;
	FTimerHandle DashTimerHandle;
	FVector DashVector;

	void DashEnter();
	void DashUpdate();
	void DashExit();

	UFUNCTION(BlueprintCallable)
		void DashCancel();


	//Wallrun
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wallrun)
	float WallrunDetectDistance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wallrun)
	float WallrunFrontDetectDistance;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wallrun)
	float WallrunMinSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wallrun)
	float WallrunFriction;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Wallrun)
	bool WallrunIsRightWallRunning;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Wallrun)
	FVector WallrunDirection;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Wallrun)
	FVector WallDirection;

	bool WallrunDetect(FHitResult &Hit, FVector &TravelDir, bool CheckLeft);
	void Wallrun(); //Button press to start wallrunning?
	void WallrunEnter();
	void WallrunExit();
	void WallrunUpdate();

	bool WallDetect();

	//Wallstick
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Wallstick)
	float WallstickDetectDistance;

	bool WallstickDetect(FHitResult &Hit);
	void Wallstick();
	void WallstickEnter();
	void WallstickExit();
	void WallstickUpdate();

	//Ragdoll
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ragdoll)
	bool IsRagdolled;

	void Ragdoll();
	void Stand();
	
#pragma endregion
	
	float GetHorizontalSpeed();

	UFUNCTION(BlueprintCallable)
	void OrientateToCamera();


	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

