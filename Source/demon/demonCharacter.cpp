// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "demonCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/GameEngine.h"

//////////////////////////////////////////////////////////////////////////
// AdemonCharacter

AdemonCharacter::AdemonCharacter()
{
	CapsuleComponent = GetCapsuleComponent();
	// Set size for collision capsule
	CapsuleComponent->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	
	//IKTraceDistance = 50.0f;
	//IKScale = GetActorTransform().GetScale3D().Z;
	//IKTraceDistance = CapsuleComponent->GetScaledCapsuleHalfHeight() * IKScale;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AdemonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AdemonCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AdemonCharacter::StopJumping);

	PlayerInputComponent->BindAction("Roll", IE_Pressed, this, &AdemonCharacter::Roll);
	PlayerInputComponent->BindAction("Roll", IE_Released, this, &AdemonCharacter::StopRolling);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AdemonCharacter::Attack);
	PlayerInputComponent->BindAction("Attack", IE_Released, this, &AdemonCharacter::StopAttacking);

	PlayerInputComponent->BindAxis("MoveForward", this, &AdemonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AdemonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AdemonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AdemonCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AdemonCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AdemonCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AdemonCharacter::OnResetVR);
}


//float AdemonCharacter::IKFootTrace(float TraceDistance, FName SocketName)
//{
//	FVector SocketLocation = GetMesh()->GetSocketLocation(SocketName);
//	FVector ActorLocation = GetActorLocation();
//	FVector Start{ SocketLocation.X, SocketLocation.Y, ActorLocation.Z };
//	FVector End{ SocketLocation.X, SocketLocation.Y, ActorLocation.Z - TraceDistance };
//	FHitResult hit;
//	FCollisionQueryParams colQP;
//	colQP.AddIgnoredActor(this);
//
//	bool DidHit = GetWorld()->SweepSingleByChannel(hit, Start, End, FQuat::Identity, ECollisionChannel::ECC_Visibility,
//		CapsuleComponent->GetCollisionShape(), colQP, FCollisionResponseParams::FCollisionResponseParams(ECollisionResponse::ECR_Ignore));
//
//	if (DidHit) {
//		GEngine->AddOnScreenDebugMessage(1, GetWorld()->TimeSeconds, FColor::Green, "Hit!");
//		float Length;
//		FVector Dir;
//		(End - hit.Location).ToDirectionAndLength(Dir, Length);
//		return Length;
//	}
//	else {
//		GEngine->AddOnScreenDebugMessage(1, GetWorld()->TimeSeconds, FColor::Green, "No Hit!");
//		return 0.0f;
//	}
//}

void AdemonCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AdemonCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AdemonCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AdemonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AdemonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AdemonCharacter::MoveForward(float Value)
{
	Move(Value, EAxis::X);
}

void AdemonCharacter::MoveRight(float Value)
{
	Move(Value, EAxis::Y);
}

void AdemonCharacter::Move(float Value, EAxis::Type Type) {
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(Type);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AdemonCharacter::Roll()
{
	isRolling = true;
}

void AdemonCharacter::StopRolling()
{
	isRolling = false;
}

void AdemonCharacter::Attack()
{
	isAttacking = true;
}

void AdemonCharacter::StopAttacking()
{
	isAttacking = false;
}

//void AdemonCharacter::Jump()
//{
//	Super::Jump();
//	isJumping = true
//};
//
//void AdemonCharacter::StopJumping()
//{
//	Super::StopJumping();
//	isJumping = false;
//}
