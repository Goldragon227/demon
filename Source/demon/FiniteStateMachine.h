// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UObject/ObjectMacros.h"
#include "FiniteStateMachine.generated.h"



class FSMState
{	
public:
	DECLARE_DELEGATE(Action)
	Action OnEnter;
	Action OnExit;
	Action OnUpdate;

	FSMState(){}

	FSMState(AActor * Instigator,  FName Update, FName Enter, FName Exit) {
		OnEnter.BindUFunction(Instigator, Enter);
		OnUpdate.BindUFunction(Instigator, Update);
		OnExit.BindUFunction(Instigator, Exit);
	}

	template<class UserClass>
	FSMState(UserClass* Object, 
		typename Action::TUObjectMethodDelegate< UserClass >::FMethodPtr Update,
		typename Action::TUObjectMethodDelegate< UserClass >::FMethodPtr Enter,
		typename Action::TUObjectMethodDelegate< UserClass >::FMethodPtr Exit) {
		OnEnter.BindUObject(Object, Enter);
		OnUpdate.BindUObject(Object, Update);
		OnExit.BindUObject(Object, Exit);
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEMON_API UFiniteStateMachine : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFiniteStateMachine();

	//Add State
	bool Add(uint8 Key, FSMState State);
	FSMState * GetState(uint8 Key);
	//ChangeState
	bool ChangeState(uint8 Key);
	//Start
	bool Start(uint8 Key);
	bool Resume();
	void Stop();
	uint8 GetCurrentState();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	FSMState::Action CurrentAction;
	FSMState * CurrentState;
	uint8 CurrentStateName;
	TMap<uint8, FSMState> * States;
	bool isRunning;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
};
