// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FistCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/GameEngine.h"
#include "FistMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "FiniteStateMachine.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "DrawDebugHelpers.h"
#include "Math/UnrealMathUtility.h"

//////////////////////////////////////////////////////////////////////////
// AFistCharacter

//AMyCharacter::AMyCharacter(const FObjectInitializer& ObjectInitializer) 
//: Super(ObjectInitializer.SetDefaultSubobjectClass<UMyMovementComponent>(ACharacter::CharacterMovementComponentName))

AFistCharacter::AFistCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UFistMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	CapsuleComponent = GetCapsuleComponent();
	// Set size for collision capsule
	CapsuleComponent->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;


	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//IKTraceDistance = 50.0f;
	//IKScale = GetActorTransform().GetScale3D().Z;
	//IKTraceDistance = CapsuleComponent->GetScaledCapsuleHalfHeight() * IKScale;

	MaxPunchChargeTime = 5.0f;
	MaxPunchDistance = 2000.f;

	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);

	SetActorTickEnabled(true);
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
	PrimaryActorTick.bStartWithTickEnabled = true;

	stateMachine = CreateDefaultSubobject<UFiniteStateMachine>(TEXT("Statemachine"));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_Dash, FSMState(this, &AFistCharacter::DashUpdate, &AFistCharacter::DashEnter, &AFistCharacter::DashExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_BoostCharge, FSMState(this, &AFistCharacter::BoostChargeUpdate, &AFistCharacter::BoostChargeEnter, &AFistCharacter::BoostChargeExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_BoostPunch, FSMState(this, &AFistCharacter::BoostPunchUpdate, &AFistCharacter::BoostPunchEnter, &AFistCharacter::BoostPunchExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_BoostPunchReverse, FSMState(this, &AFistCharacter::BoostPunchReverseUpdate, &AFistCharacter::BoostPunchReverseEnter, &AFistCharacter::BoostPunchReverseExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_BoostPunchDown, FSMState(this, &AFistCharacter::BoostVerticalDownUpdate, &AFistCharacter::BoostVerticalDownEnter, &AFistCharacter::BoostVerticalDownExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_BoostPunchUp, FSMState(this, &AFistCharacter::BoostVerticalUpUpdate, &AFistCharacter::BoostVerticalUpEnter, &AFistCharacter::BoostVerticalUpExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_Wallrun, FSMState(this, &AFistCharacter::WallrunUpdate, &AFistCharacter::WallrunEnter, &AFistCharacter::WallrunExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_Wallstick, FSMState(this, &AFistCharacter::WallstickUpdate, &AFistCharacter::WallstickEnter, &AFistCharacter::WallstickExit));
	stateMachine->Add((uint8)EFistCharacterEnum::FC_Ground, FSMState());
	stateMachine->GetState((uint8)EFistCharacterEnum::FC_Ground)->OnUpdate.BindUObject(this, &AFistCharacter::GroundUpdate);
	stateMachine->Start((uint8)EFistCharacterEnum::FC_Ground);

	ChargePunchSpeed = 2400;
	ChargeTime = 0.25f;
	DashSpeed = 2400;
	DashAcceleration = 4096.0f;
	DashTime = 0.25f;

	PunchVerticalSpeed = 2400;
	PunchVerticalTime = 0.25f;

	GetCharacterMovement()->MaxFlySpeed = DashSpeed;

	IKLeftFootName = FName("ik_foot_lSocket");
	IKRightFootName = FName("ik_foot_rSocket");
	IKTraceDistance = 200.0f;
	IKInterpSpeed = 10.0f;

	IKHeadName = FName("head");

	CapsuleHalfHeightScaled = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	//45 = X Pitch // 90 = Y Roll // 0 = Z Yaw
	IKHeadAdjustment = FRotator(90, 0, 45);
	HeadBounds = { -90,90,-90,90};
	IKHeadLookLocation = FVector::ZeroVector;
	IKHeadInterpSpeed = 10;

	WallrunMinSpeed = 1200;
	WallrunDetectDistance = 50;
	WallrunFrontDetectDistance = 80;
	WallrunFriction = 0.1f;

	WallstickDetectDistance = 50;
}
/*
AFistCharacter::AFistCharacter()
{
	CharacterMovementComponentName = "FistMovementComponent";

	CapsuleComponent = GetCapsuleComponent();
	// Set size for collision capsule
	CapsuleComponent->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
	
	//IKTraceDistance = 50.0f;
	//IKScale = GetActorTransform().GetScale3D().Z;
	//IKTraceDistance = CapsuleComponent->GetScaledCapsuleHalfHeight() * IKScale;

	MaxPunchChargeTime = 5.0f;
	MaxPunchDistance = 2000.f;
}
*/

//////////////////////////////////////////////////////////////////////////
// Input

void AFistCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFistCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFistCharacter::StopJumping);

	PlayerInputComponent->BindAction("Roll", IE_Pressed, this, &AFistCharacter::Dodge);
	PlayerInputComponent->BindAction("Roll", IE_Released, this, &AFistCharacter::StopDodging);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AFistCharacter::BoostPunch);
	//PlayerInputComponent->BindAction("Attack", IE_Released, this, &AFistCharacter::StopChargePunch);

	PlayerInputComponent->BindAction("Attack2", IE_Pressed, this, &AFistCharacter::BoostPunchReverse);
	//PlayerInputComponent->BindAction("Attack2", IE_Pressed, this, &AFistCharacter::ChargePunchReverse);
	//PlayerInputComponent->BindAction("Attack2", IE_Released, this, &AFistCharacter::StopChargePunch);

	PlayerInputComponent->BindAction("AttackUp", IE_Pressed, this, &AFistCharacter::PunchUp);
	PlayerInputComponent->BindAction("AttackDown", IE_Pressed, this, &AFistCharacter::PunchDown);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFistCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFistCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFistCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFistCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AFistCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AFistCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFistCharacter::OnResetVR);
}

void AFistCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	IKLeftFootLength = FMath::FInterpTo(IKLeftFootLength, IKFootTrace(IKTraceDistance, IKLeftFootName), DeltaTime, IKInterpSpeed);
	IKRightFootLength = FMath::FInterpTo(IKRightFootLength, IKFootTrace(IKTraceDistance, IKRightFootName), DeltaTime, IKInterpSpeed);

	IKHeadLookRotation = FMath::RInterpTo(IKHeadLookRotation, IKGetHeadRotation(IKHeadName, IKHeadLookLocation), DeltaTime, IKHeadInterpSpeed);

	if (!GetMovementComponent()->GetLastInputVector().IsZero()) {
		LastNonZeroMovementInput = GetMovementComponent()->GetLastInputVector();
	}
}

#pragma region IK

float AFistCharacter::IKFootTrace(float TraceDistance, FName SocketName)
{
	FVector SocketLocation = GetMesh()->GetSocketLocation(SocketName);
	FVector ActorLocation = GetActorLocation();
	FVector Start{ SocketLocation.X, SocketLocation.Y, ActorLocation.Z };
	FVector End{ SocketLocation.X, SocketLocation.Y, ActorLocation.Z - TraceDistance };

	FHitResult hit;
	//const FName TraceTag("DebugTrace");
	//GetWorld()->DebugDrawTraceTag = TraceTag;
	FCollisionQueryParams colQP;
	colQP.AddIgnoredActor(this);
	//colQP.TraceTag = TraceTag;
	GetWorld()->LineTraceSingleByChannel(hit, Start, End, ECollisionChannel::ECC_Visibility, colQP);

	if (hit.bBlockingHit) {
		float Length;
		FVector Dir;
		Length = ((End - hit.Location).Z + GetCapsuleComponent()->GetScaledCapsuleHalfHeight() + 10.0f) * -1;
		return Length;
	}
	else {
		return 0.0f;
	}
}

FRotator AFistCharacter::IKGetHeadRotation(FName HeadSocketName, FVector LookAtPoint)
{

	FVector SocketLocation = GetMesh()->GetSocketLocation(HeadSocketName);
	//FRotator SocketRotation = GetMesh()->GetSocketRotation(HeadSocketName);
	FVector Dir = (LookAtPoint - SocketLocation);

	FVector YawDir = Dir;
	YawDir.Z = 0;
	YawDir.Normalize();

	FVector PitchDir = Dir;
	//PitchDir.X = 0;
	//PitchDir.Y = 0;
	PitchDir.Normalize();
	//FRotator Retval = UKismetMathLibrary::FindLookAtRotation(SocketLocation, LookAtPoint);

	float YawAngle = FMath::RadiansToDegrees(acosf(FVector::DotProduct(GetActorForwardVector(), YawDir)));
	YawAngle = FMath::Clamp(YawAngle, HeadBounds.YawMin, HeadBounds.YawMax) * -FMath::Sign(GetActorRotation().Yaw);

	float PitchAngle = FMath::RadiansToDegrees(acosf(FVector::DotProduct(GetActorForwardVector(), PitchDir)));
	PitchAngle = FMath::Clamp(PitchAngle, HeadBounds.PitchMin, HeadBounds.PitchMax) * FMath::Sign(Dir.Z);
	if (PitchAngle == HeadBounds.PitchMax || PitchAngle == HeadBounds.PitchMin) {
		PitchAngle = 0;
	}

	FRotator Retval = GetActorForwardVector().Rotation() + FRotator::MakeFromEuler({ 0,PitchAngle,YawAngle });

	//DrawDebugLine(GetWorld(), SocketLocation, (SocketLocation + (YawDir * 100)), FColor::Blue, false, 0.1);
	//DrawDebugLine(GetWorld(), SocketLocation, (SocketLocation + (PitchDir * 100)), FColor::Yellow, false, 0.1);
	//DrawDebugLine(GetWorld(), SocketLocation, LookAtPoint, FColor::Red, false, 0.1);
	//if (GEngine) {
	//	GEngine->AddOnScreenDebugMessage(-1, 0.05f, FColor::Blue, Retval.ToCompactString());
	//	GEngine->AddOnScreenDebugMessage(-1, 0.05f, FColor::Blue, FString::SanitizeFloat(YawAngle));
	//	GEngine->AddOnScreenDebugMessage(-1, 0.05f, FColor::Blue, FString::SanitizeFloat(PitchAngle));
	//	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(IKLeftFootLength));
	//}
	Retval = FRotator(Retval.Pitch + IKHeadAdjustment.Pitch, Retval.Yaw + IKHeadAdjustment.Yaw, IKHeadAdjustment.Roll);

	Retval = UKismetMathLibrary::FindLookAtRotation(SocketLocation, LookAtPoint);

	//90s rotate the skeletons head to be oriented right
	//return Dir.Rotation();
	return Retval;
}

#pragma endregion

#pragma region Locomotion

void AFistCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFistCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AFistCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AFistCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFistCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AFistCharacter::MoveForward(float Value)
{
	switch ((EFistCharacterEnum)stateMachine->GetCurrentState()) {
	case EFistCharacterEnum::FC_Wallrun:
		AddMovementInput(WallrunDirection, Value);
		break;
	default:
		Move(Value, EAxis::X);
		break;
	}
}

void AFistCharacter::MoveRight(float Value)
{
	switch ((EFistCharacterEnum)stateMachine->GetCurrentState()) {
	case EFistCharacterEnum::FC_Wallrun:
		break;
	default:
		Move(Value, EAxis::Y);
		break;
	}
}

void AFistCharacter::Move(float Value, EAxis::Type Type) {
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(Type);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

#pragma endregion


#pragma region Keys
void AFistCharacter::Dodge()
{
	if (!isDashing) {
		stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Dash);
	}
}

void AFistCharacter::StopDodging()
{
}

void AFistCharacter::ChargePunch()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunch);
}

void AFistCharacter::StopChargePunch()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunch);
}

#pragma endregion


#pragma region Statemachine

void AFistCharacter::BoostPunch()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunch);
}

void AFistCharacter::BoostPunchReverse()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunchReverse);
}

void AFistCharacter::PunchUp()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunchUp);
}

void AFistCharacter::PunchDown()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_BoostPunchDown);
}

void AFistCharacter::GroundUpdate() {
	FVector Dir;
	FHitResult Hit;
	if (WallrunDetect(Hit, Dir, false) || WallrunDetect(Hit, Dir, true)) {
		if (GetHorizontalSpeed() > WallrunMinSpeed) {
			stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Wallrun);
		}
	}

	if (WallstickDetect(Hit)) {
		WallDirection = -Hit.Normal;
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 2, FColor::Yellow, WallDirection.ToCompactString());
		}
		stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Wallstick);
	}
}

void AFistCharacter::BoostChargeUpdate()
{
	ChargeTime += GetWorld()->DeltaTimeSeconds;
	OrientateToCamera();
}

void AFistCharacter::BoostChargeEnter()
{
	isBoostCharging = true;
	ChargeTime = 0;
	//Slow time
	//Snap Player facing direction to the Camera
	//Directional Movement
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.4);
	GetCharacterMovement()->bOrientRotationToMovement = false;
}

void AFistCharacter::BoostChargeExit()
{
	isBoostCharging = false;
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1);
}


void AFistCharacter::BoostPunchEnter() {
	BoostPunchEnter(1);
}
void AFistCharacter::BoostPunchUpdate() {
	BoostPunchUpdate(1);
}
void AFistCharacter::BoostPunchExit() {
	BoostPunchExit(1);
}

void AFistCharacter::BoostPunchEnter(int DirModifier)
{
	isBoostPunching = true;
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	//LaunchCharacter(FollowCamera->GetForwardVector() * MaxPunchDistance, false, false);
	DashVector = FollowCamera->GetForwardVector() * DirModifier;
	GetCharacterMovement()->Velocity = DashVector * ChargePunchSpeed;

	OrientateToCamera();

	GetWorld()->GetTimerManager().SetTimer(BoostPunchTimerHandle, this, &AFistCharacter::BoostPunchCancel, ChargeTime, false);
}

void AFistCharacter::BoostPunchUpdate(int DirModifier)
{
}

void AFistCharacter::BoostPunchExit(int DirModifier)
{
	GetWorld()->GetTimerManager().ClearTimer(BoostPunchTimerHandle);
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
	GetCharacterMovement()->bOrientRotationToMovement = true;
	isBoostPunching = false;
}

void AFistCharacter::BoostPunchReverseEnter()
{
	BoostPunchEnter(-1);
}

void AFistCharacter::BoostPunchReverseUpdate()
{
	BoostPunchUpdate(-1);
}

void AFistCharacter::BoostPunchReverseExit()
{
	BoostPunchExit(-1);
}

void AFistCharacter::BoostVerticalEnter(int DirModifier)
{
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	FVector Dir = GetActorUpVector() * DirModifier;
	GetCharacterMovement()->Velocity = Dir * PunchVerticalSpeed;
	GetWorld()->GetTimerManager().SetTimer(BoostPunchTimerHandle, this, &AFistCharacter::BoostPunchCancel, PunchVerticalTime, false);
}

void AFistCharacter::BoostVerticalExit(int DirModifier)
{
	GetWorld()->GetTimerManager().ClearTimer(BoostPunchTimerHandle);
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
	//GetCharacterMovement()->Velocity = GetActorUpVector() * DirModifier * PunchVerticalSpeed;
}

void AFistCharacter::BoostVerticalUpdate(int DirModifier)
{
	
}

void AFistCharacter::BoostVerticalUpEnter()
{
	BoostVerticalEnter(1);
}

void AFistCharacter::BoostVerticalUpExit()
{
	BoostVerticalExit(1);
}

void AFistCharacter::BoostVerticalUpUpdate()
{
	BoostVerticalUpdate(1);
}

void AFistCharacter::BoostVerticalDownEnter()
{
	BoostVerticalEnter(-1);
}

void AFistCharacter::BoostVerticalDownExit()
{
	BoostVerticalExit(-1);
}

void AFistCharacter::BoostVerticalDownUpdate()
{
	BoostVerticalUpdate(-1);
}

void AFistCharacter::BoostPunchCancel()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Ground);
}

void AFistCharacter::DashEnter() {
	isDashing = true;
	GetCharacterMovement()->Velocity = FVector::ZeroVector;
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	//OldWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
	//GetCharacterMovement()->MaxWalkSpeed = DashSpeed;
	//OldWalkAcceleration = GetCharacterMovement()->MaxAcceleration;
	//GetCharacterMovement()->MaxAcceleration = DashAcceleration;

	DashVector = GetCharacterMovement()->GetLastInputVector();

	if (DashVector.IsZero()) {
		FVector FaceDir = FollowCamera->GetForwardVector(); FaceDir.Z = 0;
		DashVector = FaceDir;
	}
	else {
		SetActorRotation(FRotator(0, GetCharacterMovement()->GetLastInputVector().Rotation().Yaw, 0));
	}
	GetCharacterMovement()->Velocity = DashVector * DashSpeed;


	//if (GEngine) {
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, DashVector.ToCompactString());
	//}

	//if (GEngine) {
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, GetActorRotation().ToCompactString());
	//}
	GetWorld()->GetTimerManager().SetTimer(DashTimerHandle, this, &AFistCharacter::DashCancel, DashTime, false);
}
void AFistCharacter::DashUpdate() {
	//
	FVector Dir = GetCharacterMovement()->GetLastInputVector();
	if (!Dir.IsZero()) {
		DashVector = Dir;
	}
	AddMovementInput(DashVector);
}

void AFistCharacter::DashExit() {
	GetWorld()->GetTimerManager().ClearTimer(DashTimerHandle);
	isDashing = false;
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Walking);
	//GetCharacterMovement()->MaxWalkSpeed = OldWalkSpeed;
	//GetCharacterMovement()->MaxAcceleration = OldWalkAcceleration;
	//GetCharacterMovement()->AirControl = 0.2f;
}

void AFistCharacter::DashCancel() {
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Ground);
}

bool AFistCharacter::WallrunDetect(FHitResult &Hit, FVector &TravelDir, bool CheckLeft)
{
	FVector Start = GetActorLocation();
	FCollisionQueryParams colQP;
	colQP.AddIgnoredActor(this);
	//Check on each side of the character for a wall
	TravelDir = FVector::ZeroVector;
	if (CheckLeft) {
		FVector LeftEnd = GetActorLocation() + (GetActorRightVector() * -WallrunDetectDistance);
		GetWorld()->LineTraceSingleByChannel(Hit, Start, LeftEnd, ECollisionChannel::ECC_Visibility, colQP);
		DrawDebugLine(GetWorld(), Start, LeftEnd, FColor::Red, false, 0.1);

		if (Hit.bBlockingHit) {
			TravelDir = FVector::CrossProduct(FVector::CrossProduct(LeftEnd - Start, Hit.Normal), Hit.Normal);
			TravelDir.Normalize();
			//DrawDebugLine(GetWorld(), Hit.ImpactPoint, Cross, FColor::Green, false, 0.1);
			//if (GEngine) {
			//	GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TravelDir.ToCompactString());
			//}
		}
	}
	else {
		FVector RightEnd = GetActorLocation() + (GetActorRightVector() * WallrunDetectDistance);
		GetWorld()->LineTraceSingleByChannel(Hit, Start, RightEnd, ECollisionChannel::ECC_Visibility, colQP);
		DrawDebugLine(GetWorld(), Start, RightEnd, FColor::Blue, false, 0.1);
		if (Hit.bBlockingHit) {
			TravelDir = FVector::CrossProduct(FVector::CrossProduct(RightEnd - Start, Hit.Normal), Hit.Normal);
			TravelDir.Normalize();
			//DrawDebugLine(GetWorld(), Hit.ImpactPoint, Cross, FColor::Green, false, 0.1);
			/*if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, Cross.ToCompactString());
			}*/
		}
		
	}
	return  Hit.bBlockingHit;
}

void AFistCharacter::Wallrun()
{
	stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Wallrun);
}

void AFistCharacter::WallrunEnter()
{
	//On attach to the wall get the direction the player is going and only let the keys change how fast the player is going
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	GetCharacterMovement()->BrakingDecelerationFlying = WallrunFriction;

	FHitResult Hit;
	if (WallrunDetect(Hit, WallrunDirection, false)) {
		WallrunIsRightWallRunning = true;
	}
	else {
		WallrunDetect(Hit, WallrunDirection, true);
		WallrunIsRightWallRunning = false;
	}

	//Hit.Normal

	//Attach player to the wall
	//Make keys control how fast you are going
	WallDirection = (-Hit.Normal);
	GetMovementComponent()->Velocity = WallrunDirection * GetHorizontalSpeed();
}

void AFistCharacter::WallrunExit()
{
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
	GetCharacterMovement()->BrakingDecelerationFlying = 0;
}

void AFistCharacter::WallrunUpdate()
{
	FHitResult Hit;
	FVector Dir;
	if (WallrunDetect(Hit, Dir, false) || WallrunDetect(Hit, Dir, true)) {
		if (GetHorizontalSpeed() < WallrunMinSpeed) {
			if (WallDetect()) {
				stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Wallstick);
			}
			else {
				stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Ground);
			}
		} 
	}
	else {
		if (WallDetect()) {
			stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Wallstick);
		}
		else {
			stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Ground);
		}
		
	}
}

bool AFistCharacter::WallDetect()
{
	FHitResult Hit;
	FVector Start = GetActorLocation();
	FVector End = Start + (WallDirection * WallrunDetectDistance);
	FCollisionQueryParams colQP;
	colQP.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_Visibility, colQP);
	DrawDebugLine(GetWorld(), Start, End, FColor::Yellow, false, 0.1);
	return Hit.bBlockingHit;
}

bool AFistCharacter::WallstickDetect(FHitResult &Hit)
{
	FVector Start = GetActorLocation();
	FVector End = Start + (GetActorForwardVector() * WallstickDetectDistance);
	
	FCollisionQueryParams colQP;
	colQP.AddIgnoredActor(this);
	GetWorld()->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_Visibility, colQP);
	return Hit.bBlockingHit;
}



void AFistCharacter::Wallstick() {

}

void AFistCharacter::WallstickEnter() {
	//Stick the player to the wall and stop movement
	//Pressing a movement button will have the player jump off in that direction relative to the camera
	/*FHitResult Hit;
	if (WallstickDetect(Hit)) {
		WallDirection = (-Hit.ImpactNormal);
	}*/

	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Flying);
	GetCharacterMovement()->Velocity = FVector::ZeroVector;

}
void AFistCharacter::WallstickExit() {
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);
}
void AFistCharacter::WallstickUpdate() {
	
	if (!WallDetect()) {
		stateMachine->ChangeState((uint8)EFistCharacterEnum::FC_Ground);
	}
}

void AFistCharacter::Ragdoll()
{
}

void AFistCharacter::Stand()
{
}

float AFistCharacter::GetHorizontalSpeed()
{
	FVector Dir;
	float Length;
	FVector Vel = GetVelocity(); Vel.Z = 0;
	Vel.ToDirectionAndLength(Dir, Length);
	return Length;
}

void AFistCharacter::OrientateToCamera()
{
	FVector FaceDir = FollowCamera->GetForwardVector(); FaceDir.Z = 0;
	SetActorRotation(FaceDir.Rotation());
}

#pragma endregion
